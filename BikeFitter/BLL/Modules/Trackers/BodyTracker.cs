﻿using System;
using System.Linq;
using System.Drawing;
using System.Threading.Tasks;
using System.Collections.Generic;
using Emgu.CV;
using BLL.Enums;
using BLL.Models;

namespace BLL.Modules.Trackers
{
    public class BodyTracker : IDisposable
    {
        private List<BodyPartTracker> _bodyParts;

        public BodyTracker()
        {
            _bodyParts = new List<BodyPartTracker>();
        }


        public void StartTrackingBodyPart(BodyPartType bodyPartType, Mat cvFrame, Rectangle trackingRect)
        {
            BodyPartTracker bodyPart = new BodyPartTracker(bodyPartType);

            bodyPart.StartTracking(cvFrame, trackingRect);

            _bodyParts.Add(bodyPart);
        }


        public void UpdateTrackedBodyParts(Mat newFrame)
        {
            Parallel.ForEach(_bodyParts.ToList(), tracker =>
            {
                if (tracker.IsTrackingActive())
                {
                    tracker.Update(newFrame);
                }
            });

            //foreach (BodyPartTracker tracker in _bodyParts.ToList())
            //{
            //    if (tracker.IsTrackingActive())
            //    {
            //        tracker.Update(newFrame);
            //    }
            //}
        }

        public List<BodyPartModel> GetBodyParts()
        {
            return _bodyParts.Select(x => new BodyPartModel
            {
                Type = x.Type,
                Position = x.GetCenter()
            }).ToList();
        }

        public void StopTracking()
        {
            foreach (var bodyPart in _bodyParts)
            {
                bodyPart.StopTracking();
            }
        }

        public void Dispose()
        {
            StopTracking();
            _bodyParts.Clear();
            _bodyParts = null;
        }

    }
}
