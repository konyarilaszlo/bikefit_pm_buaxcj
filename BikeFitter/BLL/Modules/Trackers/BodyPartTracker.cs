﻿using Emgu.CV.Tracking;
using BLL.Enums;

namespace BLL.Modules.Trackers
{
    public class BodyPartTracker : TrackablePoint<TrackerKCF>
    {
        public BodyPartType Type { get; set; }


        public BodyPartTracker(BodyPartType type)
        {
            Type = type;
        }



    }
}
