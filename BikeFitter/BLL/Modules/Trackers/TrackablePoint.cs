﻿using System;
using System.Drawing;
using Emgu.CV;
using Emgu.CV.Tracking;
using Emgu.CV.Structure;

namespace BLL.Modules.Trackers
{
    public abstract class TrackablePoint<T> : IDisposable where T : Tracker
    {
        private Tracker _tracker;

        protected Rectangle trackingRect;

        private Mat PrepImageForTracking(Mat cvFrame)
        {
            var cvImg = cvFrame.ToImage<Hsv, byte>();
            var currentCvFrame = cvImg.InRange(new Hsv(210, 80, 80), new Hsv(260, 255, 255));

            currentCvFrame = currentCvFrame.Erode(4);
            currentCvFrame = currentCvFrame.Dilate(4);

            return currentCvFrame.Mat;
        }

        public void StartTracking(Mat cvFrame, Rectangle initialTrackingRect)
        {
            if (IsTrackingActive())
            {
                _tracker.Dispose();
            }

            Init(PrepImageForTracking(cvFrame), initialTrackingRect);
        }

        public void StopTracking()
        {
            _tracker.Dispose();
        }

        public virtual void Init(Mat cvFrame, Rectangle initialRect)
        {
            if (_tracker != null)
            {
                _tracker.Dispose();
            }


            if (typeof(T) == typeof(TrackerCSRT))
            {
                _tracker = new TrackerCSRT();
            }
            else if (typeof(T) == typeof(TrackerKCF))
            {
                _tracker = new TrackerKCF(0.05f, 0.8f);
            }

            _tracker.Init(PrepImageForTracking(cvFrame), initialRect);
        }

        public virtual void Update(Mat cvFrame)
        {
            _tracker.Update(PrepImageForTracking(cvFrame), out trackingRect);

            Console.WriteLine(trackingRect);
        }

        public virtual bool IsTrackingActive()
        {
            return _tracker != null;
        }

        public virtual Point GetCenter()
        {
            return GetRectCenter(trackingRect);
        }

        public void Dispose()
        {
            StopTracking();
        }


        #region HelperMethods

        private Point GetRectCenter(Rectangle rect)
        {
            return new Point(rect.Left + rect.Width / 2,
                 rect.Top + rect.Height / 2);
        }

        #endregion
    }
}
