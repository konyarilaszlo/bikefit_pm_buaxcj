﻿using System.Collections.Generic;
using Emgu.CV;
using Emgu.CV.Structure;
using BLL.Extensions;
using BLL.ViewModels;
using BLL.Interfaces.Display;
using BLL.Models;

namespace BLL.Modules.Display
{
    public class FitterDisplayHandler : IFitterDisplay
    {
        private static FitterDisplayHandler displayHandler;

        private FitterViewModel _fitterModel { get; set; }

        private FitterDisplayHandler()
        {
            _fitterModel = new FitterViewModel();
        }

        public static FitterDisplayHandler Get()
        {
            if (displayHandler == null)
            {
                displayHandler = new FitterDisplayHandler();
            }

            return displayHandler;
        }

        public void Refresh(Mat cvFrame, List<BodyPartModel> bodyParts)
        {
            cvFrame = DrawBodyParts(cvFrame, bodyParts);

            _fitterModel.CurrentFrameToDisplay = cvFrame.Bitmap.ToBitmapSource();
        }

        private Mat DrawBodyParts(Mat cvFrame, List<BodyPartModel> bodyParts)
        {
            var image = cvFrame.ToImage<Bgr, byte>();

            foreach (BodyPartModel bodyPart in bodyParts)
            {
                var marker = new CircleF(bodyPart.Position, 30);

                image.Draw(marker, new Bgr(0, 0, 255), 5);
            }

            return image.Mat;
        }

        public FitterViewModel GetFitterModel()
        {
            return _fitterModel;
        }

        public void Dispose()
        {

        }
    }
}
