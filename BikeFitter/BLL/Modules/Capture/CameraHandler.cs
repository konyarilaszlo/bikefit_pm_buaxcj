﻿using System;
using Emgu.CV;
using Emgu.CV.CvEnum;
using BLL.Interfaces.Capture;

namespace BLL.Modules.Capture
{
    public class CameraHandler : ICameraCapture
    {
        private const int CAMERA_WIDTH = 1280;
        private const int CAMERA_HEIGHT = 720;

        public event EventHandler FrameProcessed;

        private Mat _cvMatImage;

        private VideoCapture _capture;


        public CameraHandler()
        {
            _cvMatImage = new Mat();
        }

        public void StartCapturing(int cameraNumber = 1)
        {
            _capture = new VideoCapture(cameraNumber);

            _capture.SetCaptureProperty(CapProp.FrameWidth, CAMERA_WIDTH);
            _capture.SetCaptureProperty(CapProp.FrameHeight, CAMERA_HEIGHT);
            _capture.ImageGrabbed += ProcessFrame;

            _capture.Start();
        }

        public void StopCapturing()
        {
            _capture.Stop();
        }

        public Mat GetLastProcessedFrame()
        {
            return _cvMatImage;
        }

        private void ProcessFrame(object sender, EventArgs e)
        {
            _capture.Retrieve(_cvMatImage);



            if (FrameProcessed != null)
            {
                FrameProcessed(this, EventArgs.Empty);
            }
        }

        public void Dispose()
        {
            _capture.Stop();
            _capture.Dispose();
        }
    }
}
