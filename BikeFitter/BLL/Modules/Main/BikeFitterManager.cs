﻿using BLL.Enums;
using BLL.Interfaces.Capture;
using BLL.Interfaces.Display;
using BLL.Modules.Capture;
using BLL.Modules.Display;
using BLL.Modules.Trackers;
using BLL.ViewModels;
using Emgu.CV;
using System;
using System.Drawing;

namespace BLL.Modules.Main
{
    public class BikeFitterManager : IDisposable
    {
        private ICameraCapture _cameraHandler;
        private IFitterDisplay _displayHandler;
        private BodyTracker _bodyTracker;


        public BikeFitterManager()
        {
            _cameraHandler = new CameraHandler();
            _bodyTracker = new BodyTracker();
            _displayHandler = FitterDisplayHandler.Get();
        }



        public void Start()
        {
            _cameraHandler.StartCapturing(3);
            _cameraHandler.FrameProcessed += OnFrameProcessed;
        }

        public void StartTrackingBodyPart(BodyPartType bodyPartType, Rectangle trackingRect)
        {
            _bodyTracker.StartTrackingBodyPart(bodyPartType, _cameraHandler.GetLastProcessedFrame(), trackingRect);
        }

        private void OnFrameProcessed(object sender, EventArgs e)
        {
            var cameraHandler = (CameraHandler)sender;

            Mat processedFrame = cameraHandler.GetLastProcessedFrame();

            _bodyTracker.UpdateTrackedBodyParts(processedFrame);
            _displayHandler.Refresh(processedFrame, _bodyTracker.GetBodyParts());
        }

        public FitterViewModel GetFitterViewModel()
        {
            return _displayHandler.GetFitterModel();
        }

        public void Dispose()
        {
            _cameraHandler.Dispose();
            _displayHandler.Dispose();

        }
    }
}
