﻿namespace BLL.Enums
{
    public enum BodyPartType
    {
        head,
        shoulder,
        elbow,
        hand,
        hip,
        knee,
        feet
    }
}
