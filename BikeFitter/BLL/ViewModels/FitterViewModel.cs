﻿using System.ComponentModel;
using System.Windows.Media.Imaging;

namespace BLL.ViewModels
{
    public class FitterViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private BitmapSource currentFrameToDisplay;

        public BitmapSource CurrentFrameToDisplay
        {
            get
            {
                return this.currentFrameToDisplay;
            }

            set
            {
                value.Freeze();
                this.currentFrameToDisplay = value;
                OnPropertyChanged(nameof(CurrentFrameToDisplay));
            }
        }


        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
