﻿using Emgu.CV;
using System;

namespace BLL.Interfaces.Capture
{
    public interface ICameraCapture : IDisposable
    {
        event EventHandler FrameProcessed;
        void StartCapturing(int cameraNumber);
        void StopCapturing();
        Mat GetLastProcessedFrame();
    }
}
