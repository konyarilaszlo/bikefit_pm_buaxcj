﻿using System;
using System.Collections.Generic;
using Emgu.CV;
using BLL.Models;
using BLL.ViewModels;

namespace BLL.Interfaces.Display
{
    public interface IFitterDisplay : IDisposable
    {
        void Refresh(Mat cvFrame, List<BodyPartModel> bodyParts);
        FitterViewModel GetFitterModel();
    }
}
