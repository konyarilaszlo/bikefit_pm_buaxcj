﻿using System.Drawing;
using BLL.Enums;

namespace BLL.Models
{
    public class BodyPartModel
    {
        public Point Position { get; set; }

        public BodyPartType Type { get; set; }

    }
}
