﻿using System;
using System.Drawing;
using System.Windows;
using System.Windows.Input;
using System.ComponentModel;
using System.Windows.Controls;
using BLL.Enums;
using BLL.ViewModels;
using BLL.Modules.Main;

namespace BikeFitter
{
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        private const int CAMERA_WIDTH = 1280;
        private const int CAMERA_HEIGHT = 720;

        public event PropertyChangedEventHandler PropertyChanged;
        public FitterViewModel Model { get; set; }


        private BikeFitterManager _bikeFitter;

        bool mouseDown = false;
        System.Windows.Point mouseDownPos;
        System.Windows.Point mouseDownPosReplativeToImage;

        BodyPartType? selectedBodyPartTypeToAdd;

        #region Properties

        private string statusText = null;

        public string StatusText
        {
            get
            {
                return this.statusText;
            }

            set
            {
                if (this.statusText != value)
                {
                    this.statusText = value;

                    if (this.PropertyChanged != null)
                    {
                        this.PropertyChanged(this, new PropertyChangedEventArgs("StatusText"));
                    }
                }
            }
        }

        #endregion


        public MainWindow()
        {
            InitializeComponent();

            _bikeFitter = new BikeFitterManager();
            Model = _bikeFitter.GetFitterViewModel();

            this.DataContext = Model;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            _bikeFitter.Start();
        }

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            mouseDownPos = e.GetPosition(theGrid);
            mouseDownPosReplativeToImage = e.GetPosition(imageControl);
            if (mouseDownPosReplativeToImage.X <= imageControl.ActualWidth && mouseDownPosReplativeToImage.Y <= imageControl.ActualHeight)
            {
                mouseDown = true;
                theGrid.CaptureMouse();


                Canvas.SetLeft(selectionBox, mouseDownPos.X);
                Canvas.SetTop(selectionBox, mouseDownPos.Y);
                selectionBox.Width = 0;
                selectionBox.Height = 0;

                selectionBox.Visibility = Visibility.Visible;
            }
        }

        private void Grid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (mouseDown)
            {
                mouseDown = false;
                theGrid.ReleaseMouseCapture();


                selectionBox.Visibility = Visibility.Collapsed;

                System.Windows.Point mouseUpPos = e.GetPosition(theGrid);

                var mousUpReplativeToImage = e.GetPosition(imageControl);

                int x = (int)mouseDownPosReplativeToImage.X;
                int y = (int)mouseDownPosReplativeToImage.Y;
                int width = Math.Abs((int)Math.Round(mousUpReplativeToImage.X - mouseDownPosReplativeToImage.X));
                int height = Math.Abs((int)Math.Round(mousUpReplativeToImage.Y - mouseDownPosReplativeToImage.Y));



                double widthRatio = CAMERA_WIDTH / imageControl.ActualWidth;
                double heightRatio = CAMERA_HEIGHT / imageControl.ActualHeight;

                x = (int)Math.Round(widthRatio * x);
                y = (int)Math.Round(heightRatio * y);

                width = (int)Math.Round(widthRatio * width);
                height = (int)Math.Round(heightRatio * height);

                Rectangle selectedAreaToTrack = new Rectangle(x, y, width, height);

                if (selectedBodyPartTypeToAdd.HasValue)
                {
                    _bikeFitter.StartTrackingBodyPart(selectedBodyPartTypeToAdd.Value, selectedAreaToTrack);
                }
            }
        }

        private void Grid_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseDown)
            {

                System.Windows.Point mousePos = e.GetPosition(theGrid);

                if (mouseDownPos.X < mousePos.X)
                {
                    Canvas.SetLeft(selectionBox, mouseDownPos.X);
                    selectionBox.Width = mousePos.X - mouseDownPos.X;
                }
                else
                {
                    Canvas.SetLeft(selectionBox, mousePos.X);
                    selectionBox.Width = mouseDownPos.X - mousePos.X;
                }

                if (mouseDownPos.Y < mousePos.Y)
                {
                    Canvas.SetTop(selectionBox, mouseDownPos.Y);
                    selectionBox.Height = mousePos.Y - mouseDownPos.Y;
                }
                else
                {
                    Canvas.SetTop(selectionBox, mousePos.Y);
                    selectionBox.Height = mouseDownPos.Y - mousePos.Y;
                }
            }
        }

        private void MainWindow_Closing(object sender, CancelEventArgs e)
        {
            _bikeFitter.Dispose();
        }


        private void ScreenshotButton_Click(object sender, RoutedEventArgs e)
        {
            /*  if (this.CurrentFrame != null)
              {

                  BitmapEncoder encoder = new PngBitmapEncoder();

                  encoder.Frames.Add(BitmapFrame.Create(this.CurrentFrame));

                  string time = System.DateTime.Now.ToString("hh'-'mm'-'ss", CultureInfo.CurrentUICulture.DateTimeFormat);

                  string path = Path.Combine("export images", "KinectScreenshot-Color-" + time + ".png");

                  try
                  {
                      using (FileStream fs = new FileStream(path, FileMode.Create))
                      {
                          encoder.Save(fs);
                      }

                      this.StatusText = string.Format("Saved screenshot to {0}", path);
                  }
                  catch (IOException)
                  {
                      this.StatusText = string.Format("Saved screenshot to {0}", path);
                  }
              }*/
        }

        private void btnAddBodyPart_Click(object sender, RoutedEventArgs e)
        {
            var btn = (Button)sender;

            BodyPartType type = (BodyPartType)Enum.Parse(typeof(BodyPartType), Convert.ToString(btn.Tag));

            btn.IsEnabled = false;

            selectedBodyPartTypeToAdd = type;
        }
    }
}

