# BikeFit projektmunka Konyári László BUAXCJ

A képfeldolgozáshoz Emgu CV keretrendszert használom, ami egy C# opencv wrapper. Az élő kép bármilyen kamerából kinyerhető amit a windows kezelni tud. A program moduláris felépítésű, 3 fő részből áll amik a munkafolyamat egyet fázisait valósítják meg
  - Capture
  - Track
  - Display
  
# Capture 
A feldolgozandő képkockák kinyerését a ICameraCapture interface-t megvalósító CameraHandler osztály végzi. A feldolgozott képkockák eseményvezért módon kerülnek tovább.

# Track
Egy adott testrész nyomon követéséhez kikell jelölni, hogy a testrészhez tartozó marker hol található a képen, egér lenyomva tartásával van erre mód. Amennyiben sikeres volt a követés inicializálása piros kör jelöli a követett területet. A követésre alapértelmezésül ad egy implementációt a TrackablePoint generikus osztály, ebből származik BodyPartTracker ami ezt bővíti tovább. Az összes testrész menedzselését a BodyTracker végzi minden alkalommal frissíti az egyes trackerek pozícióját amikor új frame érkezik. A követéshez szükséges előfeldolgozás is itt történik meg, kép küszöbölése stb.

# Display
Az open cv által használt Mat objektumból létrehozza azt a képet amit a wpf is megtud jeleníteni. Berajzolja a követett testrészekhez tartozó köröket amik jelzik az aktuális pozíciót. Minden további a kép ábrázolásához szükséges manipuláci itt kerül implementálásra.

# BikeFitterManager
Végül pedig az előbbiekben említett modulokat, tehát az egész üzleti logikát ez az osztály tartja egyben és menedzseli hogy minden a helyére kerüljön. Ez a felépítés nagy rugalmasságot ad amennyiben bármelyik modul logikáját nagyban érintő módosításokra lenne szükség, ugyanis az egyes modulok nem függnek egymástől. Az wpfben indítottól fő száltól különböző szálon kerülnek végrehajtásra a folyamat erőforrásköltséges lépései, ezért az ablak használható marad a folyamattól függetlenül is, mozgatható és átméterezhető stb.